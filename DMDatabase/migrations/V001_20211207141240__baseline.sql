SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
PRINT N'Creating [dbo].[DM_INVOICE_LINE]'
GO
CREATE TABLE [dbo].[DM_INVOICE_LINE]
(
[invoice_number] [nvarchar] (10) NOT NULL,
[inventory_item_id] [nvarchar] (10) NOT NULL,
[invoice_line_quantity] [int] NULL,
[invoice_line_sale_price] [decimal] (10, 2) NULL
)
GO
PRINT N'Creating primary key [PK__DM_INVOI__D69CED104FAB1E1D] on [dbo].[DM_INVOICE_LINE]'
GO
ALTER TABLE [dbo].[DM_INVOICE_LINE] ADD CONSTRAINT [PK__DM_INVOI__D69CED104FAB1E1D] PRIMARY KEY CLUSTERED ([invoice_number], [inventory_item_id])
GO
PRINT N'Creating [dbo].[DM_INVOICE_LINE_HISTORY]'
GO
CREATE TABLE [dbo].[DM_INVOICE_LINE_HISTORY]
(
[identCol] [int] NOT NULL IDENTITY(1, 1),
[invoice_number] [nvarchar] (6) NOT NULL,
[item_id] [nvarchar] (6) NOT NULL,
[quantity] [int] NULL
)
GO
PRINT N'Creating primary key [PK__DM_INVOI__2DE3C94A7A686635] on [dbo].[DM_INVOICE_LINE_HISTORY]'
GO
ALTER TABLE [dbo].[DM_INVOICE_LINE_HISTORY] ADD CONSTRAINT [PK__DM_INVOI__2DE3C94A7A686635] PRIMARY KEY CLUSTERED ([identCol])
GO
PRINT N'Creating trigger [dbo].[IL_trig1] on [dbo].[DM_INVOICE_LINE]'
GO
create trigger [dbo].[IL_trig1]
on [dbo].[DM_INVOICE_LINE] after insert, update
AS
BEGIN
  DECLARE @invNum integer
  DECLARE @itemID integer
  DECLARE @itemQty integer
  Select @invNum=invoice_number, @itemID=inventory_item_id, @itemQty=invoice_line_quantity from DM_INVOICE_LINE;
  insert into DM_INVOICE_LINE_HISTORY (invoice_number,item_id,quantity) 
     values (@invNum,@itemID,@itemQty);
END
GO
PRINT N'Creating [dbo].[DM_EMPLOYEE]'
GO
CREATE TABLE [dbo].[DM_EMPLOYEE]
(
[person_id] [int] NOT NULL,
[assignment_id] [int] NOT NULL,
[emp_id] [nvarchar] (50) NULL,
[first_name] [nvarchar] (40) NULL,
[last_name] [nvarchar] (40) NULL,
[full_name] [nvarchar] (40) NULL,
[birth_date] [datetime] NULL,
[gender] [nvarchar] (1) NULL,
[title] [nvarchar] (10) NULL,
[emp_data] [nvarchar] (100) NULL
)
GO
PRINT N'Creating index [empInd1] on [dbo].[DM_EMPLOYEE]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [empInd1] ON [dbo].[DM_EMPLOYEE] ([person_id], [emp_id])
GO
PRINT N'Creating [dbo].[DM_EMP_AUDIT]'
GO
CREATE TABLE [dbo].[DM_EMP_AUDIT]
(
[identCol] [int] NOT NULL IDENTITY(1, 1),
[person_id] [int] NOT NULL,
[assignment_id] [int] NOT NULL,
[emp_id] [nvarchar] (10) NULL,
[first_name] [nvarchar] (40) NULL,
[last_name] [nvarchar] (40) NULL,
[full_name] [nvarchar] (40) NULL
)
GO
PRINT N'Creating primary key [PK__DM_EMP_A__2DE3C94AFABC9B1F] on [dbo].[DM_EMP_AUDIT]'
GO
ALTER TABLE [dbo].[DM_EMP_AUDIT] ADD CONSTRAINT [PK__DM_EMP_A__2DE3C94AFABC9B1F] PRIMARY KEY CLUSTERED ([identCol])
GO
PRINT N'Creating trigger [dbo].[EMP_trig1] on [dbo].[DM_EMPLOYEE]'
GO

create trigger [dbo].[EMP_trig1]
on [dbo].[DM_EMPLOYEE] after update
AS
BEGIN
  DECLARE @person_id integer
  DECLARE @assignment_id integer
  DECLARE @emp_id nvarchar(10)
  DECLARE @first_name nvarchar(40)
  DECLARE @last_name nvarchar(40)
  DECLARE @full_name nvarchar(40)
  Select @person_id=person_id, @assignment_id=assignment_id, @emp_id=emp_id, @first_name=first_name, @last_name=last_name, @full_name=full_name from DM_EMPLOYEE;
  insert into DM_EMP_AUDIT (person_id,assignment_id, emp_id,first_name,last_name,full_name) 
     values (@person_id,@assignment_id, @emp_id,@first_name,@last_name,@full_name);
END
GO
PRINT N'Creating [dbo].[DM_CUSTOMER]'
GO
CREATE TABLE [dbo].[DM_CUSTOMER]
(
[customer_id] [nvarchar] (10) NOT NULL,
[customer_firstname] [nvarchar] (60) NULL,
[customer_lastname] [nvarchar] (60) NULL,
[customer_gender] [nvarchar] (1) NULL,
[customer_company_name] [nvarchar] (60) NULL,
[customer_street_address] [nvarchar] (60) NULL,
[customer_region] [nvarchar] (60) NULL,
[customer_country] [nvarchar] (60) NULL,
[customer_email] [nvarchar] (60) NULL,
[customer_telephone] [nvarchar] (60) NULL,
[customer_zipcode] [nvarchar] (60) NULL,
[credit_card_type_id] [nvarchar] (2) NULL,
[customer_credit_card_number] [nvarchar] (60) NULL
)
GO
PRINT N'Creating primary key [PK__DM_CUSTO__CD65CB856FB6F313] on [dbo].[DM_CUSTOMER]'
GO
ALTER TABLE [dbo].[DM_CUSTOMER] ADD CONSTRAINT [PK__DM_CUSTO__CD65CB856FB6F313] PRIMARY KEY CLUSTERED ([customer_id])
GO
PRINT N'Creating [dbo].[DM_CUSTOMER_NOTES]'
GO
CREATE TABLE [dbo].[DM_CUSTOMER_NOTES]
(
[customer_note_id] [int] NOT NULL,
[customer_id] [nvarchar] (10) NOT NULL,
[customer_firstname] [nvarchar] (60) NULL,
[customer_lastname] [nvarchar] (60) NULL,
[customer_notes_entry_date] [datetime] NOT NULL,
[customer_note] [nvarchar] (2000) NULL
)
GO
PRINT N'Creating primary key [PK_DM_CUSTOMER_NOTES] on [dbo].[DM_CUSTOMER_NOTES]'
GO
ALTER TABLE [dbo].[DM_CUSTOMER_NOTES] ADD CONSTRAINT [PK_DM_CUSTOMER_NOTES] PRIMARY KEY CLUSTERED ([customer_note_id])
GO
PRINT N'Creating [dbo].[DM_CREDIT_CARD_TYPE]'
GO
CREATE TABLE [dbo].[DM_CREDIT_CARD_TYPE]
(
[credit_card_type_id] [nvarchar] (2) NOT NULL,
[credit_card_type_name] [nvarchar] (60) NULL
)
GO
PRINT N'Creating primary key [PK__DM_CREDI__F7653008FB95C1DE] on [dbo].[DM_CREDIT_CARD_TYPE]'
GO
ALTER TABLE [dbo].[DM_CREDIT_CARD_TYPE] ADD CONSTRAINT [PK__DM_CREDI__F7653008FB95C1DE] PRIMARY KEY CLUSTERED ([credit_card_type_id])
GO
PRINT N'Creating [dbo].[DM_INVOICE]'
GO
CREATE TABLE [dbo].[DM_INVOICE]
(
[invoice_number] [nvarchar] (10) NOT NULL,
[invoice_date] [datetime] NULL,
[invoice_customer_id] [nvarchar] (60) NULL
)
GO
PRINT N'Creating primary key [PK__DM_INVOI__8081A63B37E8AE0E] on [dbo].[DM_INVOICE]'
GO
ALTER TABLE [dbo].[DM_INVOICE] ADD CONSTRAINT [PK__DM_INVOI__8081A63B37E8AE0E] PRIMARY KEY CLUSTERED ([invoice_number])
GO
PRINT N'Creating [dbo].[DM_INVENTORY_ITEM]'
GO
CREATE TABLE [dbo].[DM_INVENTORY_ITEM]
(
[inventory_item_id] [nvarchar] (10) NOT NULL,
[inventory_item_name] [nvarchar] (60) NULL
)
GO
PRINT N'Creating primary key [PK__DM_INVEN__61D4B2B49DDAA32E] on [dbo].[DM_INVENTORY_ITEM]'
GO
ALTER TABLE [dbo].[DM_INVENTORY_ITEM] ADD CONSTRAINT [PK__DM_INVEN__61D4B2B49DDAA32E] PRIMARY KEY CLUSTERED ([inventory_item_id])
GO
PRINT N'Creating [dbo].[DM_ASSIGNMENT]'
GO
CREATE TABLE [dbo].[DM_ASSIGNMENT]
(
[assignment_id] [int] NOT NULL,
[person_id] [int] NOT NULL,
[emp_id] [nvarchar] (10) NULL,
[emp_jobtitle] [nvarchar] (100) NULL,
[assignment_notes] [nvarchar] (1000) NULL
)
GO
PRINT N'Creating index [asgnInd1] on [dbo].[DM_ASSIGNMENT]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [asgnInd1] ON [dbo].[DM_ASSIGNMENT] ([person_id], [assignment_id])
GO
PRINT N'Creating [dbo].[DM_CUSTOMER_ASXML_IDAttr]'
GO
CREATE TABLE [dbo].[DM_CUSTOMER_ASXML_IDAttr]
(
[customer_id] [varchar] (10) NOT NULL,
[customer_data] [xml] NULL
)
GO
PRINT N'Creating primary key [PK_DM_CUSTOMER_ASXML_IDAttr] on [dbo].[DM_CUSTOMER_ASXML_IDAttr]'
GO
ALTER TABLE [dbo].[DM_CUSTOMER_ASXML_IDAttr] ADD CONSTRAINT [PK_DM_CUSTOMER_ASXML_IDAttr] PRIMARY KEY CLUSTERED ([customer_id])
GO
PRINT N'Creating [dbo].[DM_CUSTOMER_CONTACTS]'
GO
CREATE TABLE [dbo].[DM_CUSTOMER_CONTACTS]
(
[CONTACT_ID] [int] NOT NULL IDENTITY(1, 1),
[CONTACT_PERSON] [xml] NOT NULL CONSTRAINT [DF__DM_CUSTOM__CONTA__693CA210] DEFAULT ('<Company />')
)
GO
PRINT N'Creating primary key [PK__DM_CUSTO__99DE425840154D79] on [dbo].[DM_CUSTOMER_CONTACTS]'
GO
ALTER TABLE [dbo].[DM_CUSTOMER_CONTACTS] ADD CONSTRAINT [PK__DM_CUSTO__99DE425840154D79] PRIMARY KEY CLUSTERED ([CONTACT_ID])
GO
PRINT N'Creating [dbo].[DM_SUPPLIERS]'
GO
CREATE TABLE [dbo].[DM_SUPPLIERS]
(
[supplier_id] [int] NOT NULL,
[supplier_name] [nvarchar] (60) NULL
)
GO
PRINT N'Creating primary key [PK__DM_SUPPL__6EE594E87D1DD15C] on [dbo].[DM_SUPPLIERS]'
GO
ALTER TABLE [dbo].[DM_SUPPLIERS] ADD CONSTRAINT [PK__DM_SUPPL__6EE594E87D1DD15C] PRIMARY KEY CLUSTERED ([supplier_id])
GO
PRINT N'Adding foreign keys to [dbo].[DM_CUSTOMER]'
GO
ALTER TABLE [dbo].[DM_CUSTOMER] ADD CONSTRAINT [CU_FK] FOREIGN KEY ([credit_card_type_id]) REFERENCES [dbo].[DM_CREDIT_CARD_TYPE] ([credit_card_type_id])
GO
PRINT N'Adding foreign keys to [dbo].[DM_CUSTOMER_NOTES]'
GO
ALTER TABLE [dbo].[DM_CUSTOMER_NOTES] ADD CONSTRAINT [CN_FK] FOREIGN KEY ([customer_id]) REFERENCES [dbo].[DM_CUSTOMER] ([customer_id])
GO
PRINT N'Adding foreign keys to [dbo].[DM_INVOICE_LINE]'
GO
ALTER TABLE [dbo].[DM_INVOICE_LINE] ADD CONSTRAINT [I4_FK] FOREIGN KEY ([inventory_item_id]) REFERENCES [dbo].[DM_INVENTORY_ITEM] ([inventory_item_id])
GO
ALTER TABLE [dbo].[DM_INVOICE_LINE] ADD CONSTRAINT [I3_FK] FOREIGN KEY ([invoice_number]) REFERENCES [dbo].[DM_INVOICE] ([invoice_number])
GO
