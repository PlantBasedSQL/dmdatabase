CREATE TABLE [dbo].[DM_INVENTORY_ITEM]
(
[inventory_item_id] [nvarchar] (10) NOT NULL,
[inventory_item_name] [nvarchar] (60) NULL
)
GO
ALTER TABLE [dbo].[DM_INVENTORY_ITEM] ADD CONSTRAINT [PK__DM_INVEN__61D4B2B49DDAA32E] PRIMARY KEY CLUSTERED ([inventory_item_id])
GO
