CREATE TABLE [dbo].[DM_CUSTOMER_NOTES]
(
[customer_note_id] [int] NOT NULL,
[customer_id] [nvarchar] (10) NOT NULL,
[customer_firstname] [nvarchar] (60) NULL,
[customer_lastname] [nvarchar] (60) NULL,
[customer_notes_entry_date] [datetime] NOT NULL,
[customer_note] [nvarchar] (2000) NULL
)
GO
ALTER TABLE [dbo].[DM_CUSTOMER_NOTES] ADD CONSTRAINT [PK_DM_CUSTOMER_NOTES] PRIMARY KEY CLUSTERED ([customer_note_id])
GO
ALTER TABLE [dbo].[DM_CUSTOMER_NOTES] ADD CONSTRAINT [CN_FK] FOREIGN KEY ([customer_id]) REFERENCES [dbo].[DM_CUSTOMER] ([customer_id])
GO
