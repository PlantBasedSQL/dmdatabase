CREATE TABLE [dbo].[DM_CREDIT_CARD_TYPE]
(
[credit_card_type_id] [nvarchar] (2) NOT NULL,
[credit_card_type_name] [nvarchar] (60) NULL
)
GO
ALTER TABLE [dbo].[DM_CREDIT_CARD_TYPE] ADD CONSTRAINT [PK__DM_CREDI__F7653008FB95C1DE] PRIMARY KEY CLUSTERED ([credit_card_type_id])
GO
