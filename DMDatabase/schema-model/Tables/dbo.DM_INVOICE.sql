CREATE TABLE [dbo].[DM_INVOICE]
(
[invoice_number] [nvarchar] (10) NOT NULL,
[invoice_date] [datetime] NULL,
[invoice_customer_id] [nvarchar] (60) NULL
)
GO
ALTER TABLE [dbo].[DM_INVOICE] ADD CONSTRAINT [PK__DM_INVOI__8081A63B37E8AE0E] PRIMARY KEY CLUSTERED ([invoice_number])
GO
