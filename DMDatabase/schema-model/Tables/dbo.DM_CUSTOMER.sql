CREATE TABLE [dbo].[DM_CUSTOMER]
(
[customer_id] [nvarchar] (10) NOT NULL,
[customer_firstname] [nvarchar] (60) NULL,
[customer_lastname] [nvarchar] (60) NULL,
[customer_gender] [nvarchar] (1) NULL,
[customer_company_name] [nvarchar] (60) NULL,
[customer_street_address] [nvarchar] (60) NULL,
[customer_region] [nvarchar] (60) NULL,
[customer_country] [nvarchar] (60) NULL,
[customer_email] [nvarchar] (60) NULL,
[customer_telephone] [nvarchar] (60) NULL,
[customer_zipcode] [nvarchar] (60) NULL,
[credit_card_type_id] [nvarchar] (2) NULL,
[customer_credit_card_number] [nvarchar] (60) NULL
)
GO
ALTER TABLE [dbo].[DM_CUSTOMER] ADD CONSTRAINT [PK__DM_CUSTO__CD65CB856FB6F313] PRIMARY KEY CLUSTERED ([customer_id])
GO
ALTER TABLE [dbo].[DM_CUSTOMER] ADD CONSTRAINT [CU_FK] FOREIGN KEY ([credit_card_type_id]) REFERENCES [dbo].[DM_CREDIT_CARD_TYPE] ([credit_card_type_id])
GO
