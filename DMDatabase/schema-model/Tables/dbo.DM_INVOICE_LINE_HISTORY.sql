CREATE TABLE [dbo].[DM_INVOICE_LINE_HISTORY]
(
[identCol] [int] NOT NULL IDENTITY(1, 1),
[invoice_number] [nvarchar] (6) NOT NULL,
[item_id] [nvarchar] (6) NOT NULL,
[quantity] [int] NULL
)
GO
ALTER TABLE [dbo].[DM_INVOICE_LINE_HISTORY] ADD CONSTRAINT [PK__DM_INVOI__2DE3C94A7A686635] PRIMARY KEY CLUSTERED ([identCol])
GO
