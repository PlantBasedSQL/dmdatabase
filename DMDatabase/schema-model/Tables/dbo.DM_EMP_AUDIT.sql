CREATE TABLE [dbo].[DM_EMP_AUDIT]
(
[identCol] [int] NOT NULL IDENTITY(1, 1),
[person_id] [int] NOT NULL,
[assignment_id] [int] NOT NULL,
[emp_id] [nvarchar] (10) NULL,
[first_name] [nvarchar] (40) NULL,
[last_name] [nvarchar] (40) NULL,
[full_name] [nvarchar] (40) NULL
)
GO
ALTER TABLE [dbo].[DM_EMP_AUDIT] ADD CONSTRAINT [PK__DM_EMP_A__2DE3C94AFABC9B1F] PRIMARY KEY CLUSTERED ([identCol])
GO
